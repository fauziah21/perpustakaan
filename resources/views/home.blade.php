<head>
    <link rel="icon" href="{{asset('image/logo.png')}}" type="image/gif" sizes="16x16">
    <link rel="stylesheet" href="/css/home.css">
</head>

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-4">
            <div class="card card-item">
                <div class="card-body">
                    <img src="{{asset('image/siswa.jpg')}}" class="card-img-top" alt="">
                    <a href="/siswa" class="btn btn-primary">Siswa</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-item">
                <img src="{{asset('image/buku.jpg')}}" class="card-img-top" alt="">
                <div class="card-body">
                    <a href="/buku" class="btn btn-primary">Buku</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-item">
                <img src="{{asset('image/perpus.jpg')}}" alt="">
                <div class="card-body">
                    <a href="/transaction" class="btn btn-primary">Peminjaman Buku</a>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection