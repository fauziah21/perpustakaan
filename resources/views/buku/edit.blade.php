<head>
    <title>Buku</title>
    <link rel="icon" href="{{asset('image/logo.png')}}" type="image/gif" sizes="16x16">
</head>

@extends('adminlte.master')

@section('content')
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Edit Data Buku</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/buku/{{$book->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-body">

            <div class="form-group">
                <label for="judul">Judul</label>
                <input type="text" class="form-control" id="judul" name="judul" value="{{old('judul', $book->judul)}}" placeholder="Masukkan Judul Buku">
                @error('judul')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="pengarang">Pengarang</label>
                <input type="text" class="form-control" id="pengarang" name="pengarang" value="{{old('pengarang', $book->pengarang)}}" placeholder="Masukkan Nama Pengarang Buku">
                @error('pengarang')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="penerbit">Penerbit</label>
                <input type="text" class="form-control" id="penerbit" name="penerbit" value="{{old('penerbit', $book->penerbit)}}" placeholder="Masukkan Nama Penerbit">
                @error('penerbit')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="tahun_terbit">Tahun Terbit</label>
                <input type="text" class="form-control" id="tahun_terbit" name="tahun_terbit" value="{{old('tahun_terbit', $book->tahun_terbit)}}" placeholder="Masukkan Tahun Terbit">
                @error('tahun_terbit')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="jumlah_buku">Jumlah Buku</label>
                <input type="number" min="0" class="form-control" id="jumlah_buku" name="jumlah_buku" value="{{old('jumlah_buku', $book->jumlah_buku)}}" placeholder="Masukkan Jumlah Buku">
                @error('jumlah_buku')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>


        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Update</button>
            <a href="/buku" type="button" class="btn btn-danger">Cancel</a>
        </div>
    </form>
</div>
@endsection