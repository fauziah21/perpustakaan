<head>
    <title>Buku</title>
    <link rel="stylesheet" href="/css/siswa.css">
    <link rel="icon" href="{{asset('image/logo.png')}}" type="image/gif" sizes="16x16">
</head>

@extends('adminlte.master')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Data Buku</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        @if(session('success'))
        <div class="alert alert-success">
            {{session('success')}}
        </div>

        @endif
        <div class="row">
            <div class="col-md-6">
                <a href="/buku/create" class="btn btn-info mb-2">Tambah Data</a>
            </div>
            <div class="col-md-6">
                <form action="/buku" method="GET">
                    @csrf
                    <div class="input-group">
                        <input class="form-control " type="search" name="cariBuku" placeholder="Cari data buku">
                        <span class="input-group-prepend">
                            <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                        </span>

                    </div>
                </form>
            </div>
        </div>

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th style="width: 10px">No</th>
                    <th>Kode Buku</th>
                    <th>Judul</th>
                    <th>Pengarang</th>
                    <th>Penerbit</th>
                    <th>Tahun Terbit</th>
                    <th>Jumlah Buku</th>
                    <th style="width: 40px" colspan="2">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse($books as $key => $book)
                <tr>
                    <td>{{$books->firstItem() + $key}}</td>
                    <td>{{$book->id}}</td>
                    <td>{{$book->judul}}</td>
                    <td>{{$book->pengarang}}</td>
                    <td>{{$book->penerbit}}</td>
                    <td>{{$book->tahun_terbit}}</td>
                    <td>{{$book->jumlah_buku}}</td>
                    <td>
                        <a href="/buku/{{$book->id}}/edit" class="btn btn-primary btn-sm ml-1 mr-1">Edit</a>
                    </td>
                    <td>
                        <form action="/buku/{{$book->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="8" align="center">No Data</td>
                </tr>
                @endforelse
            </tbody>
        </table>
        <div class="float-sm-left mt-1">
            Showing
            {{ $books->firstItem() }}
            to
            {{ $books->lastItem() }}
            of
            {{ $books->total() }}
            entries
        </div>
        <div class="float-sm-right mt-2">
            {{ $books->links() }}
        </div>
    </div>

</div>
@endsection