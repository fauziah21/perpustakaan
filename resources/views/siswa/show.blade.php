<head>
    <title>siswa</title>
    <link rel="stylesheet" href="/css/main.css">
    <link rel="icon" href="{{asset('image/logo.png')}}" type="image/gif" sizes="16x16">
</head>

@extends('adminlte.master')

@section('content')
<h4>Data Siswa</h4>
<p>No Induk Siswa : {{ $student->no_induk }}</p>
<p>Nama Siswa : {{ $student->nama}}</p>
@endsection