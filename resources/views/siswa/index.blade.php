<head>
    <title>siswa</title>
    <link rel="stylesheet" href="/css/siswa.css">
    <link rel="icon" href="{{asset('image/logo.png')}}" type="image/gif" sizes="16x16">
</head>

@extends('adminlte.master')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Data Anggota Perpustakaan</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        @if(session('success'))
        <div class="alert alert-success">
            {{session('success')}}
        </div>

        @endif
        <div class="row">
            <div class="col-md-6">
                <a href="/siswa/create" class="btn btn-info mb-2">Tambah Data</a>
            </div>
            <div class="col-md-6">
                <form action="/siswa" method="GET">
                    @csrf
                    <div class="input-group">
                        <input class="form-control " type="search" name="cariSiswa" placeholder="Cari data anggota">
                        <span class="input-group-prepend">
                            <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                        </span>

                    </div>
                </form>
            </div>
        </div>

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th style="width: 10px">No</th>
                    <th>ID Anggota</th>
                    <th>Nomor Induk Siswa</th>
                    <th>Nama Siswa</th>
                    <th style="width: 40px" colspan="2">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse($students as $key => $student)
                <tr>
                    <td>{{$students->firstItem() + $key}}</td>
                    <td>{{$student->id}}</td>
                    <td>{{$student->no_induk}}</td>
                    <td>{{$student->nama}}</td>
                    <td>
                        <a href="/siswa/{{$student->id}}/edit" class="btn btn-primary btn-sm ml-1 mr-1">Edit</a>

                    </td>
                    <td>
                        <form action="/siswa/{{$student->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="5" align="center">No Data</td>
                </tr>
                @endforelse
            </tbody>
        </table>
        <div class="float-sm-left mt-1">
            Showing
            {{ $students->firstItem() }}
            to
            {{ $students->lastItem() }}
            of
            {{ $students->total() }}
            entries
        </div>
        <div class="float-sm-right mt-2">
            {{ $students->links() }}
        </div>
    </div>
    <!-- /.card-body -->
    <!-- <div class="card-footer clearfix">

        <ul class="pagination pagination-sm m-0 float-right">

            <li class="page-item"><a class="page-link" href="#">«</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">»</a></li>
        </ul>
    </div> -->
</div>
@endsection