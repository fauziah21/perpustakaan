<head>
    <title>siswa</title>
    <link rel="stylesheet" href="/css/main.css">
    <link rel="icon" href="{{asset('image/logo.png')}}" type="image/gif" sizes="16x16">
</head>

@extends('adminlte.master')

@section('content')
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Tambah Data Siswa</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/siswa" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="no_induk">Nomor Induk Siswa</label>
                <input type="text" class="form-control" id="no_induk" name="no_induk" value="{{old('no_induk', '')}}" placeholder="Masukkan Nomor Induk">
                @error('no_induk')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="nama">Nama Siswa</label>
                <input type="text" class="form-control" id="nama" name="nama" value="{{old('nama', '')}}" placeholder="Masukkan Nama Siswa">
                @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>


        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="/siswa" type="submit" class="btn btn-danger">Cancel</a>
        </div>
    </form>
</div>
@endsection