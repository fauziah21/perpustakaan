<head>
    <title>Peminjaman Buku</title>
    <link rel="icon" href="{{asset('image/logo.png')}}" type="image/gif" sizes="16x16">
</head>

@extends('adminlte.master')

@section('content')
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Tambah Data Peminjaman Buku</h3>
    </div>
    <!-- /.card-header -->
    @if (session('status'))
    <div class="alert alert-danger mt-2 mr-2 ml-2" style="text-align: center">
        {{ session('status') }}
    </div>
    @endif
    <!-- form start -->
    <form role="form" action="/transaction" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="student_id">ID Anggota</label>
                <input type="number" min="0" class="form-control" id="student_id" name="student_id" value="{{old('student_id', '')}}" placeholder="Masukkan ID Anggota">
                @error('student_id')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="book_id">Kode Buku</label>
                <input type="number" min="0" class="form-control" id="book_id" name="book_id" value="{{old('book_id', '')}}" placeholder="Masukkan Kode Buku">
                @error('book_id')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="tanggal_pinjam">Tanggal pinjam</label>
                <input type="date" class="form-control" id="tanggal_pinjam" name="tanggal_pinjam">
                @error('tanggal_pinjam')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="tanggal_balik">Tanggal balik</label>
                <input type="date" class="form-control" id="tanggal_balik" name="tanggal_balik">
                @error('tanggal_balik')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <!-- <div class="form-group">
                <label for="keterangan">Keterangan</label>
                <input type="text" class="form-control" id="keterangan" name="keterangan" value="{{old('keterangan', '')}}" placeholder="Masukkan Keterangan">
                @error('keterangan')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div> -->


        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="/transaction" type="submit" class="btn btn-danger">Cancel</a>
        </div>
    </form>
</div>
@endsection