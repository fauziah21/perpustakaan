<head>
    <title>Peminjaman Buku</title>
    <link rel="stylesheet" href="/css/transaction.css">
    <link rel="icon" href="{{asset('image/logo.png')}}" type="image/gif" sizes="16x16">
</head>

@extends('adminlte.master')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Data Peminjaman Buku</h3>
    </div>
    <div>
        <a href="/transaction"></a>
    </div>
    <!-- /.card-header -->
    <div class="card-body">


        @if(session('success'))
        <div class="container">
            <div class="row justify-content-center mt-5">
                <div class="col-md-5">
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Success!</strong> {{session('success')}}
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="alert alert-success">
            {{session('success')}}
        </div> -->

        @endif
        <div class="row">
            <div class="col-md-6">
                <div class="buttons">
                    <a href="/transaction/create" class="btn btn-outline-primary mb-2">Tambah Data</a>
                    <a href="/transaction" class="btn btn-outline-info mb-2">History</a>
                </div>
            </div>
            <div class="col-md-6">
                <form action="/transaction" method="GET">
                    @csrf
                    <div class="input-group">
                        <input class="form-control " type="search" name="cariPeminjaman" placeholder="Masukkan ID Peminjaman">
                        <span class="input-group-prepend">
                            <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th style="width: 10px">No</th>
                    <th>ID Peminjaman</th>
                    <th>ID Anggota</th>
                    <th>Nama Siswa</th>
                    <th>Judul Buku</th>
                    <th>Tanggal Pinjam</th>
                    <th>Tanggal Balik</th>
                    <th>Keterangan</th>
                    <th style="width: 40px" colspan="2">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse($transactions as $key => $transaction)
                <tr>
                    <td>{{$transactions->firstItem() + $key}}</td>
                    <td>{{$transaction->id}}</td>
                    <td>{{$transaction->students->id}}</td>
                    <td>{{$transaction->students->nama}}</td>
                    <td>{{$transaction->books->judul}}</td>
                    <td>{{$transaction->tanggal_pinjam}}</td>
                    <td>{{$transaction->tanggal_balik}}</td>
                    <td>
                        @if($transaction->tanggal_balik == null)
                        <a href="/transaction/{{$transaction->id}}/edit" class="badge bg-warning text-dark">Pengembalian</a>
                        @else
                        <span class="badge bg-success">Dikembalikan</span>
                        @endif
                    </td>
                    <td><a href="/transaction/{{$transaction->id}}/edit" class="btn btn-primary btn-sm ml-1 mr-1">Edit</a></td>
                    <td>
                        <form action="/transaction/{{$transaction->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="8" align="center">No Data</td>
                </tr>
                @endforelse
            </tbody>
        </table>
        <div class="float-sm-left mt-1">
            Showing
            {{ $transactions->firstItem() }}
            to
            {{ $transactions->lastItem() }}
            of
            {{ $transactions->total() }}
            entries
        </div>
        <div class="float-sm-right mt-2">
            {{ $transactions->links() }}
        </div>
    </div>

</div>
@endsection

@push('scripts')
<script src="js/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        window.setTimeout(function() {
            $(".alert").fadeTo(500, 0).slideUp(500, function() {
                $(this).remove();
            });
        }, 4000);
    });
</script>
@endpush