<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'books';
    protected $fillable = ['judul', 'pengarang', 'penerbit', 'tahun_terbit', 'jumlah_buku'];
}
