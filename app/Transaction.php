<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'book_student';
    protected $fillable = ["student_id", "book_id", "tanggal_pinjam", "tanggal_balik", "keterangan"];

    public function books()
    {
        return $this->belongsTo('App\Book', 'book_id');
    }

    public function students()
    {
        return $this->belongsTo('App\Student', 'student_id');
    }
}
