<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;

class SiswaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except(['index']);
    }

    public function create()
    {
        return view('siswa.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'no_induk' => 'required|unique:students',
            'nama' => 'required'

        ]);

        // $student = new Student;
        // $student->no_induk = $request->no_induk;
        // $student->nama = $request->nama;
        // $student->save();

        $student = Student::create([
            "no_induk" => $request["no_induk"],
            "nama" => $request["nama"]
        ]);

        return redirect('/siswa')->with('success', 'Data berhasil disimpan!');
    }

    public function index(Request $request)
    {
        if ($request->has('cariSiswa')) {
            $students = Student::where('nama', 'like', '%' . $request["cariSiswa"] . '%')
                ->orWhere('id', 'LIKE', '%' . $request["cariSiswa"] . '%')
                ->orWhere('no_induk', 'LIKE', '%' . $request["cariSiswa"] . '%')
                ->paginate(5);
        } else {
            $students = Student::paginate(5);
        }

        // $students = Student::all();

        return view('siswa.index', compact('students'));
    }

    // public function searchSiswa(Request $request)
    // {
    //     // dd($request->all());
    //     $search = $request->get('search');
    //     $students = Student::where('nama', 'like', '%' . $search . '%')->paginate(5);
    //     // dd($students);
    //     return view('siswa.index', compact('students'));
    // }

    public function show($id)
    {
        $student = Student::find($id);
        // dd($student);
        return view('siswa.show', compact('student'));
    }

    public function edit($id)
    {
        $student = Student::find($id);
        return view('siswa.edit', compact('student'));
    }

    public function update($id, request $request)
    {
        $request->validate([
            'no_induk' => 'required',
            'nama' => 'required'

        ]);
        $student = Student::where('id', $id)->update([
            "no_induk" => $request["no_induk"],
            "nama" => $request["nama"]
        ]);

        return redirect('/siswa')->with('success', 'Berhasil update data!');
    }

    public function destroy($id)
    {
        Student::destroy($id);
        return redirect('/siswa')->with('success', 'Data berhasil dihapus!');
    }
}
