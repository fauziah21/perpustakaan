<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;

class BukuController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    public function index(Request $request)
    {
        if ($request->has('cariBuku')) {
            $books = Book::where('judul', 'like', '%' . $request["cariBuku"] . '%')
                ->orWhere('pengarang', 'like', '%' . $request["cariBuku"] . '%')
                ->orWhere('id', 'like', '%' . $request["cariBuku"] . '%')
                ->orWhere('penerbit', 'like', '%' . $request["cariBuku"] . '%')
                ->orWhere('tahun_terbit', 'like', '%' . $request["cariBuku"] . '%')
                ->paginate(5);
        } else {
            $books = Book::paginate(5);
        }
        return  view('buku.index', compact('books'));
    }


    public function create()
    {
        return view('buku.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'pengarang' => 'required'

        ]);


        // dd($request);
        $book = Book::create([
            "judul" => $request["judul"],
            "pengarang" => $request["pengarang"],
            "penerbit" => $request["penerbit"],
            "tahun_terbit" => $request["tahun_terbit"],
            "jumlah_buku" => $request["jumlah_buku"]
        ]);


        return redirect('/buku')->with('success', 'Data berhasil disimpan!');
    }


    public function edit($id)
    {
        $book = Book::find($id);
        return view('buku.edit', compact('book'));
    }

    public function update(Request $request, $id)
    {

        $request->validate([
            'judul' => 'required',
            'pengarang' => 'required'
        ]);

        $book = Book::where('id', $id)->update([
            "judul" => $request["judul"],
            "pengarang" => $request["pengarang"],
            "penerbit" => $request["penerbit"],
            "tahun_terbit" => $request["tahun_terbit"],
            "jumlah_buku" => $request["jumlah_buku"]
        ]);

        return redirect('/buku')->with('success', 'Berhasil update data!');
    }

    public function destroy($id)
    {
        Book::destroy($id);
        return redirect('/buku')->with('success', 'Data berhasil dihapus!');
    }
}
