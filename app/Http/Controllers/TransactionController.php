<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\Student;
use App\Book;

class TransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // dd($request->all());
        if ($request->has('cariPeminjaman')) {
            $transactions = Transaction::where('id', 'like', '%' . $request["cariPeminjaman"] . '%')->paginate(5);
        } else {
            $transactions = Transaction::paginate(5);
        }

        // $students = Student::paginate(5);
        return view('transaction.index', compact('transactions'));
    }


    public function create()
    {
        $students = Student::pluck('id', 'nama');
        $books = Book::pluck('id', 'judul');
        return view('transaction.create', compact('students', 'books'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 1. cek apakah data ada di database
     * 2. jika ada boleh isi
     * 3. jika tidak ada redirect ke form create
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'student_id' => 'required',
            'book_id' => 'required',
            'tanggal_pinjam' => 'required'
        ]);

        $hasBook = Book::where('id',  $request["book_id"])->first();
        $hasStudent = Student::where('id', $request["student_id"])->first();
        // dd($hasStudent);
        if ($hasBook === null) {
            return redirect()->back()->with('status', 'Data Buku tidak tersedia');
        }
        if ($hasStudent === null) {
            return redirect()->back()->with('status', 'Data Anggota tidak tersedia');
        }

        $transaction = Transaction::create([
            "student_id" => $request["student_id"],
            "book_id" => $request["book_id"],
            "tanggal_pinjam" => $request["tanggal_pinjam"],
            "tanggal_balik" => $request["tanggal_balik"]
        ]);

        return redirect('/transaction')->with('success', 'Data berhasil disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transactions = Transaction::find($id);
        return view('transaction.edit', compact('transactions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'student_id' => 'required',
            'book_id' => 'required'
        ]);

        $hasBook = Book::where('id',  $request["book_id"])->first();
        $hasStudent = Student::where('id', $request["student_id"])->first();
        // dd($hasStudent);
        if ($hasBook === null) {
            return redirect()->back()->with('status', 'Data Buku tidak tersedia');
        }
        if ($hasStudent === null) {
            return redirect()->back()->with('status', 'Data Anggota tidak tersedia');
        }

        $transaction = Transaction::where('id', $id)->update([
            "student_id" => $request["student_id"],
            "book_id" => $request["book_id"],
            "tanggal_pinjam" => $request["tanggal_pinjam"],
            "tanggal_balik" => $request["tanggal_balik"]
        ]);
        return redirect('/transaction')->with('success', 'Data berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Transaction::destroy($id);
        return redirect('/transaction')->with('success', 'Data berhasil dihapus!');
    }

    public function tes(Request $request)
    {
        if ($request->has('cariPeminjaman')) {
            $transactions = Transaction::where('id', 'like', '%' . $request["cariPeminjaman"] . '%')->paginate(5);
        } else {
            $transactions = Transaction::where('tanggal_balik', '=', null)->paginate(5);
        }

        // $students = Student::paginate(5);
        return view('transaction.pengembalian', compact('transactions'));
    }
}
