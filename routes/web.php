<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/admin', function () {
    return view('admin');
});


Route::get('/master', function () {
    return view('adminlte.master');
});

Route::get('/siswa/create', 'SiswaController@create');
Route::post('/siswa', 'SiswaController@store');
Route::get('/siswa', 'SiswaController@index');
Route::get('/siswa/{id}', 'SiswaController@show');
Route::get('/siswa/{id}/edit', 'SiswaController@edit');
Route::put('/siswa/{id}', 'SiswaController@update');
Route::delete('/siswa/{id}', 'SiswaController@destroy');
// Route::get('/search', 'SiswaController@searchSiswa');

Route::resource('buku', 'BukuController');

Route::resource('transaction', 'TransactionController');
Route::get('/transaction-pengembalian', 'TransactionController@tes');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
